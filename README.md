In this project, I provisioned a 3-tier web application to Google Cloud Platform with Terraform. I used [`cloud-foundation-fabric modules`](https://github.com/GoogleCloudPlatform/cloud-foundation-fabric/tree/master/modules/) to perform the deployment. 

The 3-tier application layers:
- I set up a Cloud SQL instance for the data access layer.
- I use my previously developed [`REST API`](https://github.com/lakatostomi/rest-api-on-gcp) as a backend service layer.
- Due to my limited frontend knowledge the frontend layer is a simply static website deployed to Apache Webserver. I use a bash script as a startup script that sends a request to one of the REST API's endpoints through the Network load balancer. Using the result of the query the application sends requests to a middleware [`HTML REST API`](https://gitlab.com/java_projects803551/html_code_app) service which genarates an HTML website and returns the website as a String. The HTML service application is deployed to Cloud Run. 

In the infrastructure I use the following API's and services of Google Cloud:

- Cloud SQL: 
    - I create a MySQL instance with only a private IP and Private Service Connection,
    - I create a VM Bastion Host as a middleware between the backend service and the Cloud SQL instance, the backend service connects to the database through Cloud SQL Auth Proxy which is installed to the VM at startup with a simple bash script. The Bastion Host listens on the 3306 port on the range of subnet IPs only.
- Compute instance template and Managed Instance Groups
    - Both the frontend and backend services use a VM instance template to create the MIG with autoscaling and health checks.
- Internal Passthrough Network Load balancer
    - The frontend service interacts with the backend service through a network load balancer
- Cloud NAT 
    - None of the VM instances have public IP addresses, the VMs update and install packages through Cloud NAT
- Firewall rules
    - I set up `http`, `ssh`, `icmp`, `health-check`, `tcp-3306`, and `IAP` firewall rules. I connected to the Cloud SQL instance with Cloud Identity-Aware Proxy from my local MySQL Workbench. For the connection need to create a firewall rule that allows Identit-Aware Proxy Connection. 
- Cloud Run 
    - The HTML service application is deployed to Cloud Run, only internal traffic is allowed and Cloud IAM authentication is required to invoke the service.   
- Cloud Storage Bucket
    - The startup scripts are located in a bucket and the input data that is used by the backend service is also located in a public bucket.
- Custom Service accounts
    - Each service (Auth Proxy VM, Backend MIG, Frontend MIG) uses a custom service account to meet the least privilege principle.      
- Application HTTP Loadbalancer
    - None of the services are public to the internet for security reasons, therefore the Application has a single entry point through the Application Load Balancer, 
- Cloud DNS
    - I set up a DNS-managed zone and record set 

The Terraform state file is managed by GitLab and a CI/CD pipeline is also configured for the project.

I also deployed the REST webservice to another infrastructure where I use BigQuery as input source, that can be found [`here`](https://gitlab.com/terraform_projects2/web_app_on_gcp) and my static website [`here`](https://github.com/lakatostomi/first_gcp_website)