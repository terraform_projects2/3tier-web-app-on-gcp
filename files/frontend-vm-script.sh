#!/bin/bash
apt-get update && apt -y install apache2

gcloud config set compute/region europe-west4
gcloud config set compute/zone europe-west4-a

sleep 30

IP=$(gcloud compute forwarding-rules describe ilb-mig-lb-forwarding-rule --region europe-west4 --format='get(IPAddress)')
curlResult=$(curl -o /dev/null -s -w "%{http_code}\n" http://$IP:8080/api/rest/v1/countries/code?code=HUN)

echo "result status code:" $curlResult

while [[ ! $curlResult == "200" ]]; do
  echo "Network lb is not up yet!"
  sleep 2
  curlResult=$(curl -s -o /dev/null -I -w "%(http-code)" http://$IP:8080/api/rest/v1/countries/code?code=HUN)
    
done

RESPONSE=$(curl http://$IP:8080/api/rest/v1/countries/code?code=HUN)
 echo "<html><body><p>$RESPONSE</p></body></html>" | sudo tee /var/www/html/index.html
 EOF
