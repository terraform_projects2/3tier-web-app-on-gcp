#! bin/bash

curl -o cloud-sql-proxy https://storage.googleapis.com/cloud-sql-connectors/cloud-sql-proxy/v2.8.2/cloud-sql-proxy.linux.amd64
cd /
chmod +x cloud-sql-proxy
./cloud-sql-proxy --address 0.0.0.0 --port 3306 tier-web-application:europe-west4:mysql-db --private-ip --run-connection-test
