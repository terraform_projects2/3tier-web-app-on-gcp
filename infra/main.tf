locals {
  project_id = "tier-web-application"
  region     = "europe-west4"
  zone       = "europe-west4-a"
  database   = "population_data"
  vpc        = "web-app-vpc"
  subnet     = "web-app-subnet"
}

resource "google_project_service" "enable_apis" {
  for_each = toset(var.list_of_apis)
  project  = local.project_id
  service  = each.key

  disable_dependent_services = true
}

module "backend-service-account" {
  source     = "../modules/iam-service-account"
  project_id = local.project_id

  name = "backend-vm-sa"

  iam_project_roles = {
    "${local.project_id}" = [
      "roles/storage.objectViewer",
      "roles/compute.instanceAdmin"
    ]
  }
}

module "frontend-service-account" {
  source     = "../modules/iam-service-account"
  project_id = local.project_id

  name = "frontend-vm-sa"

  iam_project_roles = {
    "${local.project_id}" = [
      "roles/storage.objectViewer",
      "roles/compute.viewer"
    ]
  }
}

module "cloudsql-auth-proxy-service-account" {
  source     = "../modules/iam-service-account"
  project_id = local.project_id

  name = "cloudsql-auth-proxy-sa"

  iam_project_roles = {
    "${local.project_id}" = [
      "roles/storage.objectViewer",
      "roles/cloudsql.client"
    ]
  }
}

module "vpc" {
  source     = "../modules/net-vpc"
  project_id = local.project_id
  name       = local.vpc

  subnets = [
    {
      ip_cidr_range         = "10.164.0.0/24"
      name                  = local.subnet
      region                = local.region
      enable_private_access = true
    }
  ]

  psa_config = {
    ranges        = { app-range = "10.37.80.0/24" }
    export_routes = true
    import_routes = false
  }
}

module "vpc-firewall-rules" {
  source     = "../modules/net-vpc-firewall-yaml"
  project_id = local.project_id

  network = module.vpc.name
  config_directories = [
    "../factories/firewall-rules/common",
    "../factories/firewall-rules/dev"
  ]
}

module "data-input-bucket" {
  source     = "../modules/gcs"
  project_id = local.project_id

  name          = "web-app-data-input-bucket"
  storage_class = "REGIONAL"
  location      = local.region

  objects_to_upload = {
    input-data = {
      name         = "population-data.json"
      source       = "../files/population_data.json"
      content_type = "text/json"
    }
  }

  iam = {
    "roles/storage.objectViewer" = ["allUsers"]
  }
}

module "script-bucket" {
  source     = "../modules/gcs"
  project_id = local.project_id

  name          = "web-app-scripts-bucket"
  storage_class = "REGIONAL"
  location      = local.region

  objects_to_upload = {
    script-data-1 = {
      name         = "frontend-vm-script.sh"
      source       = "../files/frontend-vm-script.sh"
      content_type = "application/x-shellscript"
    }
    script-data-2 = {
      name         = "proxy-vm-script.sh"
      source       = "../files/proxy-vm-script.sh"
      content_type = "application/x-shellscript"
    }
  }
}

module "mysql-db" {
  source     = "../modules/cloudsql-instance"
  project_id = local.project_id

  name                        = "mysql-db"
  region                      = local.region
  availability_type           = "REGIONAL"
  database_version            = "MYSQL_8_0"
  tier                        = "db-g1-small"
  disk_size                   = 20
  deletion_protection_enabled = false

  network_config = {
    connectivity = {
      psa_config = {
        private_network = module.vpc.self_link
      }
    }
  }

  databases = [
    local.database
  ]

  depends_on = [module.vpc]
}

resource "google_sql_user" "users" {
  name     = "user"
  instance = module.mysql-db.name
  host     = "%"
  password = var.mysql_db_password
}

module "cloud-sql-auth-proxy-vm" {
  source     = "../modules/compute-vm"
  project_id = local.project_id

  description   = "Bastion VM instace with Cloud SQL Auth Proxy installed on it!"
  zone          = local.zone
  name          = "cloud-sql-auth-proxy-vm"
  instance_type = "e2-small"

  network_interfaces = [{
    network    = module.vpc.name
    subnetwork = local.subnet
    addresses  = { external = null }
  }]

  boot_disk = {
    initialize_params = {
      image = "projects/debian-cloud/global/images/family/debian-12"
      type  = "pd-ssd"
      size  = 10
    }
  }

  metadata = {
    startup-script-url = "${module.script-bucket.url}/proxy-vm-script.sh"
  }

  service_account = {
    email = module.cloudsql-auth-proxy-service-account.email
  }
}

module "cloud-nat" {
  source         = "../modules/net-cloudnat"
  project_id     = local.project_id
  region         = local.region
  name           = "web-app-cloud-nat"
  router_network = module.vpc.name
  router_name    = "app-router"
}

module "html-service-cloud-run" {
  source     = "../modules/cloud-run"
  project_id = local.project_id
  region     = local.region
  name       = "html-webservice"
  containers = {
    hello = {
      image = "gcr.io/tier-web-application/html-service/rest-api@sha256:ab1c33538cf4ed79e301b81fa74ee8613d05e01ecb5dcc2ad16fd36aaab5a885"
    }

  }
  ingress_settings = "internal"
  revision_annotations = {
    autoscaling = {
      max_scale = 2
      min_scale = 0
    }
  }
  iam = {
    "roles/run.invoker" = ["serviceaccount:${module.project-backend-service-account.email}"]
  }
}

module "gce-container" {
  source  = "terraform-google-modules/container-vm/google"
  version = "~> 2.0"

  container = {
    image = "gcr.io/tier-web-application/country-population-restapi@sha256:824b1f00fdddd5cd3002930fc77585e0ba68b99f6ea49c6bc41e9bf772ccfbc7"

    env = [
      {
        name  = "file.source.location"
        value = "https://storage.googleapis.com/${module.data-input-bucket.name}/population-data.json"
      },
      {
        name  = "spring.datasource.url"
        value = "jdbc:mysql://${module.cloud-sql-auth-proxy-vm.internal_ip}:3306/${local.database}"
      },
      {
        name  = "spring.datasource.username"
        value = "user"
      },
      {
        name  = "spring.datasource.password"
        value = var.mysql_db_password
      },
      {
        name  = "html.service.url"
        value = module.html-service-cloud-run.service
      },
    ]

    restart_policy = "Always"
  }
}

module "backend-cos-vm-template" {
  source     = "../modules/compute-vm"
  project_id = local.project_id

  description   = "VM instance template for backend MIG"
  zone          = local.zone
  name          = "backend-cos-instance-template"
  instance_type = "e2-medium"
  tags          = ["allow-http"]

  network_interfaces = [{
    network    = module.vpc.name
    subnetwork = local.subnet
    addresses  = { external = null }
  }]

  boot_disk = {
    initialize_params = {
      image = module.gce-container.source_image
      type  = "pd-balanced"
      size  = 10
    }
  }

  metadata = {
    gce-container-declaration = module.gce-container.metadata_value
  }

  labels = {
    container-vm = module.gce-container.vm_container_label
  }

  service_account = {
    email = module.backend-service-account.email
  }

  create_template = true
}

module "frontend-vm-template" {
  source     = "../modules/compute-vm"
  project_id = local.project_id

  description   = "VM instance template for frontend MIG"
  zone          = local.zone
  name          = "frontend-instance-template"
  tags          = ["allow-http", "allow-health-check"]
  instance_type = "e2-micro"

  network_interfaces = [{
    network    = module.vpc.name
    subnetwork = local.subnet
  }]

  boot_disk = {
    initialize_params = {
      image = "projects/debian-cloud/global/images/family/debian-12"
      type  = "pd-balanced"
      size  = 10
    }
  }

  metadata = {
    startup-script-url = "${module.script-bucket.url}/frontend-vm-script.sh"
  }

  service_account = {
    email = module.frontend-service-account.email
  }

  create_template = true
}

module "backend-vm-mig" {
  source     = "../modules/compute-mig"
  project_id = local.project_id

  location          = local.zone
  name              = "backend-vm-mig"
  target_size       = null
  instance_template = module.backend-cos-vm-template.template.self_link
  named_ports       = { backend-port = 8080 }

  auto_healing_policies = {
    initial_delay_sec = 30
  }

  health_check_config = {
    enable_logging = false
    http = {
      port         = 8080
      request_path = "/api/rest/v1/countries?page=0"
    }
  }

  autoscaler_config = {
    max_replicas    = 3
    min_replicas    = 1
    cooldown_period = 30
    scaling_signals = {
      cpu_utilization = {
        target = 0.75
      }
    }
  }

  update_policy = {
    minimal_action = "REPLACE"
    type           = "PROACTIVE"
    min_ready_sec  = 30
    max_surge = {
      fixed = 1
    }
  }

  depends_on = [module.mysql-db]
}

module "net-ilb" {
  source     = "../modules/net-lb-int"
  project_id = local.project_id

  description = "Internal network lb between frontend service MIG and backend service MIG"
  region      = local.region
  name        = "net-ilb"
  vpc_config = {
    network    = module.vpc.name
    subnetwork = local.subnet
  }

  forwarding_rules_config = {
    lb-forwarding-rule = {
      ports = ["8080"]
    }
  }

  backends = [{
    group = module.backend-vm-mig.group_manager.instance_group
  }]

  health_check_config = {
    check_interval_sec  = 30
    healthy_threshold   = 2
    timeout_sec         = 5
    unhealthy_threshold = 3
    http = {
      port         = 8080
      request_path = "/api/rest/v1/countries?page=0"
    }
  }
}

module "frontend-vm-mig" {
  source     = "../modules/compute-mig"
  project_id = local.project_id

  location          = local.zone
  name              = "frontend-vms-mig"
  target_size       = null
  instance_template = module.frontend-vm-template.template.self_link

  named_ports = {
    frontend-port = 80
  }

  auto_healing_policies = {
    initial_delay_sec = 30
  }

  health_check_config = {
    enable_logging      = false
    check_interval_sec  = 30
    healthy_threshold   = 2
    timeout_sec         = 10
    unhealthy_threshold = 2
    http = {
      port = 80
    }
  }

  autoscaler_config = {
    max_replicas    = 3
    min_replicas    = 1
    cooldown_period = 30
    scaling_signals = {
      cpu_utilization = {
        target = 0.8
      }
    }
  }

  update_policy = {
    minimal_action = "REPLACE"
    type           = "PROACTIVE"
    min_ready_sec  = 30
    max_surge = {
      fixed = 1
    }
  }

  depends_on = [module.net-ilb]
}

resource "google_compute_global_address" "static_ip" {
  project  = local.project_id
  provider = google
  name     = "lb-static-ip"
}

module "app-7lb" {
  source     = "../modules/net-lb-app-ext"
  project_id = local.project_id

  name     = "application-ext-7lb"
  address  = google_compute_global_address.static_ip.address
  ports    = ["80"]
  protocol = "HTTP"

  backend_service_configs = {
    frontend-service = {
      backends = [
        { backend = module.frontend-vm-mig.group_manager.instance_group },
      ]
      protocol   = "HTTP"
      port_name  = "frontend-port"
      enable_cdn = true
    }
  }

  health_check_configs = {
    default = {
      http = { port = 80 }
    }
  }

  urlmap_config = {
    default_service = "frontend-service"
    host_rules = [{
      hosts        = ["*"]
      path_matcher = "allpaths"
    }]
    path_matchers = {
      allpaths = {
        default_service = "frontend-service"

      }
    }
  }
}

module "public-dns" {
  source     = "../modules/dns"
  project_id = local.project_id

  description = "My managed domain"
  name        = "population-data-zone"

  zone_config = {
    domain = "population-data.com."
  }

  recordsets = {
    "A statistics" = { ttl = 300, records = ["${google_compute_global_address.static_ip.address}"] }
  }
}



