# GCP provider

provider "google" {
  credentials = file(var.sa_key)
  project     = local.project_id
  region      = local.region
}

provider "google-beta" {
  credentials = file(var.sa_key)
  project     = local.project_id
  region      = local.region
}