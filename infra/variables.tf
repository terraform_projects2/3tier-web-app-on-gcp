variable "sa_key" {
  description = "Service account key for authentication"
  sensitive   = true
}

variable "list_of_apis" {
  description = "List of Apis and services necessary for the project"
  type        = list(string)
  default = ["serviceusage.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com", "compute.googleapis.com",
    "sqladmin.googleapis.com",
    "bigquery.googleapis.com",
    "bigquerystorage.googleapis.com",
    "servicenetworking.googleapis.com",
    "dns.googleapis.com"
  ]
}

variable "mysql_db_password" {
  description = "Password for database connection!"
  type        = string
}