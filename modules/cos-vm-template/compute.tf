
resource "google_compute_instance_template" "cos_template" {
  project        = var.project_id
  name           = var.instance_template.name
  description    = var.instance_template.description
  tags           = var.instance_template.tags
  machine_type   = var.instance_template.machine_type
  can_ip_forward = var.instance_template.can_ip_forward

  disk {
    source_image = var.instance_template.source_image
    disk_type    = var.instance_template.disk_type
    disk_size_gb = var.instance_template.disk_size_gb
    boot         = var.instance_template.boot
  }

  metadata = var.instance_template.metadata

  labels = var.instance_template.labels

  network_interface {
    network    = var.instance_template.network
    subnetwork = var.instance_template.subnetwork
    access_config {
      nat_ip = var.instance_template.nat_ip
    }
  }

  service_account {
    email = var.instance_template.email
    scopes = var.instance_template.service_account_scope
  }
}
