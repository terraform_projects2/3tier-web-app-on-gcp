output "google_compute_instance_template" {
  value     = google_compute_instance_template.cos_template.id
  sensitive = false
}
