variable "project_id" {
  type = string
}

variable "instance_template" {
  type = object({
    name                  = string
    description           = string
    tags                  = list(string)
    machine_type          = string
    can_ip_forward        = optional(bool, false)
    source_image          = string
    disk_type             = string
    disk_size_gb          = number
    boot                  = optional(bool, true)
    metadata              = optional(map(string), {})
    labels                = optional(map(string), {})
    network               = string
    subnetwork            = string
    nat_ip                = optional(string, null)
    email                 = optional(string, null)
    service_account_scope = optional(list(string), ["default"])
  })
}
